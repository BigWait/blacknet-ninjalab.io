---
title: Announcement of initial distribution
---

Initial distribution starts at 07 Sep 2018 00:00:00 UTC and ends at 14 Dec 2018 00:00:00 UTC.

Details and instructions are at [IBO](../burn.html) page.
