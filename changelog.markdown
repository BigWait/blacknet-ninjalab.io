---
titleOnlyInHead: 1
---

## --Blacknet -0.2.6--


* Regression test mode
* Fix deadlock
* Add Status report details
* Fix TxPool
* UI:i18n update

## References
[blacknet.ninja-0.2.6](https://vasin.nl/blacknet-0.2.6.zip)


## --Blacknet -0.2.5--


Hard Fork V2 launched at block 485075

* Add MultiData
* Withdraw From Lease
* Simplify HTLC
* Fix reward after burn tx
* Fix SpendMultisigF
* Change to 16 second blocks
* Ui: bug fix and update
* Use blacknetjs-lib


## References
[blacknet.ninja-0.2.5](https://vasin.nl/blacknet-0.2.5.zip)


## --Blacknet -0.2.4--


* Lease amount must gt 1000 BLN
* ChainFetcher: fix race condition
* Config: add txpoolsize
* PeerDB: add network stat
* WalletDB: fix deadlock
* WalletDB: use real hash for Generated
* Ui: bug fix and update


## References
[blacknet.ninja-0.2.4](https://vasin.nl/blacknet-0.2.4.zip)


## --Blacknet -0.2.3--


* Update to kotlin 1.3.40
* Network: factor out Runtime
* Update leveldb java to 0.12
* PeerInfo: cache reads from db
* Ui: bug fix and update


## References
[blacknet.ninja-0.2.3](https://vasin.nl/blacknet-0.2.3.zip)


## --Blacknet -0.2.2--


* Implement bootstrap
* Add soft block size limit, default 1MB
* WalletDB: update broadcaster
* TxPool: implement htlc and multisig
* Api: add disconnectpeer
* GetData: fix too long packet
* Ui: bug fix and update


## References
[blacknet.ninja-0.2.2](https://vasin.nl/blacknet-0.2.2.zip)


## --Blacknet -0.2.1--


* Fix updating TxPool
* LedgerDB: fix exception
* Ui: bug fix and update


## References
[blacknet.ninja-0.2.1](https://vasin.nl/blacknet-0.2.1.zip)


## --Blacknet -0.2.0--


* APIServer: use cio engine
* Api: add address info
* Api: add shutdown and supply
* Api : add getoutleases
* Api: add block notify v2
* Api: add confirmed balance
* network: add ChainFork packet
* UndoBlock: add transaction hashes
* config: add portable mode
* Db: refactor locking
* ChainFetcher: fix JobCancellationException
* Add WalletDB
* WalletDB: optimize write on rescan
* WalletDB: rebroadcast unconfirmed transactions
* Ui: bug fix and update
* Mapedb change to leveldb


## References
[blacknet.ninja-0.2.0](https://vasin.nl/blacknet-0.2.0.zip)