---
titleOnlyInHead: 1
---

## Core Wallet (Full Node)

Prerequisite: <i class="fab fa-java"></i> Java 8 or higher ([OpenJDK](https://openjdk.java.net/install/), [Oracle Java](https://java.com/download/), or other).

* Download and unzip the latest release.
* Change directory to `blacknet/bin`
* On Windows run `blacknet.bat`
* On UN*X run `./blacknet`
* Web interface is available at [http://localhost:8283/](http://localhost:8283/)

[<i class="fas fa-file-archive"></i> Download](https://vasin.nl/blacknet-0.2.6.zip)

BLAKE2b-256 checksum CD1DF651E9C7AFF65EBABBE3FB2F9F3CFF0DA51EF92A14C06FC6FC51EE9E0CCC

## Desktop Wallet

[<i class="fab fa-apple"></i> macOS](https://gitlab.com/blacknet-ninja/blacknet-desktop/-/releases)

[<i class="fab fa-windows"></i> Windows](https://gitlab.com/blacknet-ninja/blacknet-desktop/-/releases)


## Mobile Wallet

[<i class="fab fa-google-play"></i> Google Play](https://play.google.com/store/apps/details?id=ninja.blacknet.wallet.blacknet)

[<i class="fab fa-android"></i> Android APK](https://gitlab.com/blacknet-ninja/blacknet-mobile/-/releases)

[<i class="fab fa-app-store-ios"></i> iOS](https://apps.apple.com/app/blacknet/id1489451592)
