---
titleOnlyInHead: 1
---

<h3>Source code</h3>

[<i class="fab fa-gitlab"></i> GitLab](https://gitlab.com/blacknet-ninja)


<h3>Community</h3>

[<i class="fas fa-comments"></i> #blacknet:matrix.org](https://riot.im/app/#/room/#blacknet:matrix.org)

[<i class="fab fa-bitcoin"></i> BitcoinTalk](https://bitcointalk.org/index.php?topic=469640.0)

[<i class="fab fa-reddit"></i> Reddit](https://www.reddit.com/r/blacknet)

[<i class="fas fa-rss-square"></i> Blacknet archives](atom.xml)

[<i class="fas fa-rss-square"></i> Activity in the GitLab group](https://gitlab.com/blacknet-ninja.atom)

[<i class="fab fa-qq"></i> QQ group](https://www.qq.com/) 705602427


<h3>Exchanges</h3>

[<i class="fas fa-exchange-alt"></i> AEX.plus](https://www.aex.plus/page/trade.html#/?symbol=bln_cnc)

[<i class="fas fa-exchange-alt"></i> vinex.network](https://vinex.network/market/BTC_BLN) 

[<i class="fas fa-exchange-alt"></i> citex.co.kr](https://www.citex.co.kr/#/trade/BLN_BTC) 

[<i class="fas fa-exchange-alt"></i> bitalong.com](https://www.bitalong.com/trade/index/market/bln_usdt) 


<h3>Explorers</h3>

[<i class="fas fa-cubes"></i> blnscan.io](https://www.blnscan.io/)

[<i class="fas fa-cubes"></i> blacknet.xyz](http://blacknet.xyz/)



<h3>Staking Pools</h3>

[<i class="fas fa-cubes"></i> blnpool.io](https://www.blnpool.io/)

[<i class="fas fa-cubes"></i> stakepool.xyz](http://www.stakepool.xyz/)
