---
title: Anonymous connection to network
---

Network configuration is stored in `config/blacknet.conf`

### I2P

[I2P](https://geti2p.net/) is an anonymous overlay network - a network within a network. It is intended to protect communication from dragnet surveillance and monitoring by third parties such as ISPs.

Blacknet uses SAM API that is disabled by default in I2P router.

It can be enabled in I2P Router Console -> I2P Client Configuration -> SAM application bridge

```
i2psamhost=127.0.0.1
i2psamport=7656
```

### Tor

[Tor](https://www.torproject.org/) is free software and an open network that helps you defend against traffic analysis, a form of network surveillance that threatens personal freedom and privacy, confidential business activities and relationships, and state security.

Tor control port is disabled by default in `torrc` config file.

Tor proxy for outgoing .onion connections

```
torhost=127.0.0.1
torport=9050
```

Tor proxy for outgoing TCP/IP connections

```
listen=false

proxyhost=127.0.0.1
proxyport=9050
```

Tor control port to listen on .onion address

```
torcontrol=9051
```

### TCP/IP

TCP/IP can proxified using SOCKS5 protocol

```
listen=false

proxyhost=127.0.0.1
proxyport=9050
```

or disabled

```
ipv4=false
ipv6=false
listen=false
```
